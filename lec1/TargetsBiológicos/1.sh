#!/bin/bash -x

sort target.csv > aux.txt
uniq aux.txt > target.csv

cat target.csv | awk -F":" '{print "insert into target_type (c1, c2) values ($1 , $2 );"}' > aux.txt

mv aux.txt aux.sql
