#!/bin/bash
# Convert file to dos2unix dos2unix file
files=$( echo $1 )

function bd() {
  cat $files | awk -F";" '{ sub(/\r/,"",$7); print "insert into tabla (c1, c2, c3, c4, c5, c6, c7) values (\x27" $1 "\x27,\x27" $2 "\x27,\x27" $3 "\x27," $4 "," $5 "," $6 "," $7 ");"; }' > aux.sql
  # convert to bd
  rm aux.sql
}

function json() {
  cat $files | awk -F\; '{ print "{\"station\": \"" $1 "\", \"date\": \"" $2 "\", \"time\": \"" $3 "\", \"temperature\": " $4 ", \"humidity\": " $5 ", \"radiation\": " $6 "}"}' > aux.json
  # convert to json
  cat aux.json
  rm aux.json
}

function menu() {
  # statements
  echo "Convert the file to:"
  echo "1- bd"
  echo "2- json"
  echo "3- exit"
  o=""
  read o
  if [[ $o == 1 ]]; then
      bd
  elif [[ $o == 2 ]]; then
      json
  elif [[ $o == 3 ]]; then
      exit 1
  else
      exit 1
  fi
}

# Parameter validation
if [[ $# > 1 ]]; then
    echo "Only one parameter needs to be entered."
    exit 1
# If no parameters are entered, it closes.
elif [[ $# == 0 ]]; then
    echo "Enter a parameter."
    exit 1
fi

menu
